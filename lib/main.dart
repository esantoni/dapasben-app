import 'package:dapasben/provider/providers.dart';
import 'package:dapasben/ui/check.dart';
import 'package:dapasben/utils/sessions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'ui/home.dart';
import 'ui/login.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: Providers().getProviders(),
      child: MaterialApp(
        title: 'Data Pasca Bencana',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: FutureBuilder(
          future: getTokenAuth(),
          builder: (context, snapshot) {
            if(snapshot.hasData && snapshot.data != ""){
              return Home();
            }else if(snapshot.hasError || snapshot.data == ""){
              return Login();
            }
            return Check();
          },
        ),
      ),
    );
  }

  Future getTokenAuth() async{
    return await Session().getString(Session.keyTokenAuth) ?? "";
  }
}