class ModelDisaster{
  final String id;
  final String createdAt;
  final String name;
  final String desc;

  ModelDisaster(this.id, this.createdAt, this.name, this.desc);

  Map toJson() => {
    'id': id,
    'createdAt': createdAt,
    'name': name,
    'desc': desc
  };
}