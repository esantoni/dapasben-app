class ModelMarker{
  final String id;
  final String name;
  final double lat;
  final double lng;
  final String imagePath, pic, kontak, pengungsi, kebutuhan;

  ModelMarker(this.id, this.name, this.lat, this.lng, this.imagePath, this.pic,
      this.kontak, this.pengungsi, this.kebutuhan);

  Map toJson() => {
    'id': id,
    'name': name,
    'lat': lat,
    'lng': lng,
    'imagePath': imagePath,
    'pic': pic,
    'kontak': kontak,
    'pengungsi': pengungsi,
    'kebutuhan': kebutuhan,
  };
}