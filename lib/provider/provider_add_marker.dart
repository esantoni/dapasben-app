import 'package:flutter/foundation.dart';

class ProviderAddMarker with ChangeNotifier{
  String name, pic, kontak, pengungsi, kebutuhan;

  String get getName => name;
  String get getPIC => pic;
  String get getKontak => kontak;
  String get getPengungsi => pengungsi;
  String get getKebutuhan => kebutuhan;
  bool get getValidation {
    if(name.isNotEmpty && pic.isNotEmpty && kontak.isNotEmpty &&
        pengungsi.isNotEmpty && kebutuhan.isNotEmpty){
      return true;
    }else{
      return false;
    }
  }

  set setName(String value){
    name = value;
    notifyListeners();
  }
  set setPIC(String value){
    pic = value;
    notifyListeners();
  }
  set setKontak(String value){
    kontak = value;
    notifyListeners();
  }
  set setPengungsi(String value){
    pengungsi = value;
    notifyListeners();
  }
  set setKebutuhan(String value){
    kebutuhan = value;
    notifyListeners();
  }

  setReset(){
    name = null;
    pic = null;
    kontak = null;
    pengungsi = null;
    kebutuhan = null;
  }
}