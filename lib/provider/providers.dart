import 'package:dapasben/provider/provider_add_disaster.dart';
import 'package:dapasben/provider/provider_add_marker.dart';
import 'package:dapasben/provider/provider_detail_disaster.dart';
import 'package:dapasben/provider/provider_home.dart';
import 'package:dapasben/provider/provider_login.dart';
import 'package:provider/provider.dart';

class Providers{
  getProviders(){
    return [
      ChangeNotifierProvider<ProviderLogin>(create: (context) => ProviderLogin()),
      ChangeNotifierProvider<ProviderHome>(create: (context) => ProviderHome()),
      ChangeNotifierProvider<ProviderAddDisaster>(create: (context) => ProviderAddDisaster()),
      ChangeNotifierProvider<ProviderDetailDisaster>(create: (context) => ProviderDetailDisaster()),
      ChangeNotifierProvider<ProviderAddMarker>(create: (context) => ProviderAddMarker()),
    ];
  }
}