import 'package:flutter/foundation.dart';

class ProviderHome with ChangeNotifier{
  bool isLoading;

  bool get getIsLoading => isLoading;

  set setIsLoading(bool value){
    isLoading = value;
    notifyListeners();
  }

  setReset(){
    isLoading = false;
  }
}