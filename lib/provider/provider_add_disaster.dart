import 'package:flutter/foundation.dart';

class ProviderAddDisaster with ChangeNotifier{
  String name, desc;

  String get getName => name;
  String get getDesc => desc;
  bool get getValidation {
    if(name.isNotEmpty && desc.isNotEmpty){
      return true;
    }else{
      return false;
    }
  }

  set setName(String value){
    name = value;
    notifyListeners();
  }

  set setDesc(String value){
    desc = value;
    notifyListeners();
  }

  setReset(){
    name = null;
    desc = null;
  }
}