import 'package:flutter/foundation.dart';
import 'package:latlong/latlong.dart';

class ProviderDetailDisaster with ChangeNotifier{
  bool isLoading;
  LatLng point;

  bool get getIsLoading => isLoading;
  LatLng get getPoint => point;

  set setIsLoading(bool value){
    isLoading = value;
    notifyListeners();
  }

  set setPoint(LatLng value){
    point = value;
    notifyListeners();
  }

  setReset(){
    isLoading = false;
    point = LatLng(-6.175269734325388, 106.82717425762242);
  }
}