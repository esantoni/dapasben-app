import 'package:flutter/foundation.dart';

class ProviderLogin with ChangeNotifier{
  String username, password;
  bool isHidden;

  String get getUsername => username;
  String get getPassword => password;
  bool get getIsHidden => isHidden;
  bool get getValidation {
    if(username.isNotEmpty && password.isNotEmpty){
      return true;
    }else{
      return false;
    }
  }

  set setUsername(String value){
    username = value;
    notifyListeners();
  }

  set setPassword(String value){
    password = value;
    notifyListeners();
  }

  set setIsHidden(bool value){
    isHidden = value;
    notifyListeners();
  }

  setReset(){
    username = null;
    password = null;
    isHidden = true;
  }
}