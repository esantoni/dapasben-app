import 'package:dapasben/models/model_marker.dart';
import 'package:dapasben/provider/provider_detail_disaster.dart';
import 'package:dapasben/utils/api.dart';
import 'package:dapasben/utils/my_dialog.dart';
import 'package:dapasben/utils/sessions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:latlong/latlong.dart';
import 'package:provider/provider.dart';

class DetailDisaster extends StatefulWidget {
  final String disasterId;

  DetailDisaster({Key key, @required this.disasterId}) : super(key: key);

  @override
  _DetailDisasterState createState() => _DetailDisasterState();
}

class _DetailDisasterState extends State<DetailDisaster> {
  final PopupController _popupLayerController = PopupController();
  List<MonumentMarker> listMarkers = [];

  @override
  void initState() {
    super.initState();
    final provider = Provider.of<ProviderDetailDisaster>(context, listen: false);
    provider.setReset();

    getData();
  }

  Future<Null> getData() async {
    final provider = Provider.of<ProviderDetailDisaster>(context, listen: false);

    String token = await Session().getString(Session.keyTokenAuth) ?? "";

    var jsonResponse = await Api().methodGet(Api.getMarker, token);

    if (jsonResponse != null) {
      var jsonData = jsonResponse['data'];

      listMarkers = [];
      for (var d in jsonData) {
        if(d["disasterId"].toString() == widget.disasterId){
          listMarkers.add(MonumentMarker(
            monument: ModelMarker(
              d["ID"].toString(),
              d["name"],
              d["lat"],
              d["lng"],
              "",
              d["PIC"],
              d["Kontak"],
              d["Pengungsi"],
              d["Kebutuhan"],
            ),
          ));
        }
      }

      if(listMarkers.length != 0){
        provider.setPoint = LatLng(listMarkers[0].monument.lat, listMarkers[0].monument.lng);
      }
    } else {
      MyDialog().dialogNormal(context, "Gagal Ambil Data", "Silakan ulangi beberapa saat lagi");
    }

    provider.setIsLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderDetailDisaster>(context, listen: true);
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Pasca Bencana'),
      ),
      body: provider.getIsLoading ?
      FlutterMap(
        options: MapOptions(
          plugins: <MapPlugin>[PopupMarkerPlugin()],
          center: provider.getPoint,
          zoom: 12.0,
          interactive: true,
          onTap: (point) {
            _popupLayerController.hidePopup();
            MyDialog().dialogAddLocation(context, "Tambah Marker", "Anda ingin tambahkan marker di lokasi ${point.latitude}, ${point.longitude}?", point.latitude, point.longitude, widget.disasterId);
          },
        ),
        layers: <LayerOptions>[
          TileLayerOptions(
            urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            subdomains: <String>['a', 'b', 'c'],
          ),
          PopupMarkerLayerOptions(
            markers: listMarkers,
            popupController: _popupLayerController,
            popupBuilder: (_, Marker marker) {
              if (marker is MonumentMarker) {
                return MonumentMarkerPopup(monument: marker.monument);
              }
              return Card(child: const Text('Not a monument'));
            },
          ),
        ],
      ) :
      Container(
          width: double.infinity,
          height: double.infinity,
          alignment: Alignment.center,
          child: Wrap(
            children: [
              Column(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 15.0),
                  Text("Harap tunggu...",
                      style: TextStyle(
                        color: Color(0xFF272A2D),
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center)
                ],
              )
            ],
          )
      ),
    );
  }
}

class MonumentMarker extends Marker {
  final ModelMarker monument;

  MonumentMarker({@required this.monument}) : super(
    anchorPos: AnchorPos.align(AnchorAlign.top),
    height: 25,
    width: 25,
    point: LatLng(monument.lat, monument.lng),
    builder: (BuildContext ctx) => Icon(Icons.camera_alt, size: 30),
  );
}

class MonumentMarkerPopup extends StatelessWidget {
  final ModelMarker monument;
  const MonumentMarkerPopup({Key key, this.monument}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            FadeInImage.assetNetwork(
                placeholder: "assets/logos/logo.png",
                image: monument.imagePath,
                width: 200,
            ),
            Text(monument.name.toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold)),
            Text('${monument.lat}, ${monument.lng}', style: TextStyle(fontStyle: FontStyle.italic)),
            SizedBox(height: 10),
            Text("PIC: ${monument.pic.toUpperCase()}"),
            Text("Kontak: ${monument.kontak}"),
            Text("Pengungsi: ${monument.pengungsi}"),
            Text("Kebutuhan: \n${monument.kebutuhan}", textAlign: TextAlign.center),
          ],
        ),
      ),
    );
  }
}
