import 'package:dapasben/provider/provider_add_marker.dart';
import 'package:dapasben/utils/api.dart';
import 'package:dapasben/utils/my_dialog.dart';
import 'package:dapasben/utils/sessions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddMarker extends StatefulWidget {
  final String disasterId;
  final double lat, lng;

  AddMarker({Key key, @required this.disasterId, @required this.lat, @required this.lng}) : super(key: key);

  @override
  _AddMarkerState createState() => _AddMarkerState();
}

class _AddMarkerState extends State<AddMarker> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController picController = new TextEditingController();
  TextEditingController kontakController = new TextEditingController();
  TextEditingController pengungsiController = new TextEditingController();
  TextEditingController kebutuhanController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    final provider = Provider.of<ProviderAddMarker>(context, listen: false);
    provider.setReset();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderAddMarker>(context, listen: true);
    return Scaffold(
        backgroundColor: Colors.indigo[50],
        appBar: AppBar(
          title: Text('Data Pasca Bencana'),
        ),
        body: Container(
            padding: EdgeInsets.all(8),
            child: ListView(
              children: [
                Text(
                  "TAMBAH MARKER",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20.0),
                TextField(
                  controller: nameController,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: 'Nama Marker',
                    fillColor: Colors.white,
                    filled: true,
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 2.0,
                      ),
                    ),
                    errorText: provider.getName == ""
                        ? "Harap masukan Nama Marker"
                        : null,
                  ),
                ),
                SizedBox(height: 24.0),
                TextField(
                  controller: picController,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: 'Nama PIC',
                    fillColor: Colors.white,
                    filled: true,
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 2.0,
                      ),
                    ),
                    errorText: provider.getPIC == ""
                        ? "Harap masukan Nama PIC"
                        : null,
                  ),
                ),
                SizedBox(height: 24.0),
                TextField(
                  controller: kontakController,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    hintText: 'Nomor Kontak',
                    fillColor: Colors.white,
                    filled: true,
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 2.0,
                      ),
                    ),
                    errorText: provider.getKontak == ""
                        ? "Harap masukan Nomor Kontak"
                        : null,
                  ),
                ),
                SizedBox(height: 24.0),
                TextField(
                  controller: pengungsiController,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: 'Jumlah Pengungsi',
                    fillColor: Colors.white,
                    filled: true,
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 2.0,
                      ),
                    ),
                    errorText: provider.getPengungsi == ""
                        ? "Harap masukan Jumlah Pengungsi"
                        : null,
                  ),
                ),
                SizedBox(height: 24.0),
                TextField(
                  controller: kebutuhanController,
                  textInputAction: TextInputAction.newline,
                  keyboardType: TextInputType.multiline,
                  minLines: 5,
                  maxLines: 50,
                  decoration: InputDecoration(
                    hintText: 'Kebutuhan',
                    fillColor: Colors.white,
                    filled: true,
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 2.0,
                      ),
                    ),
                    errorText: provider.getKebutuhan == ""
                        ? "Harap masukan Kebutuhan"
                        : null,
                  ),
                ),
                SizedBox(height: 24.0),
                Text(
                  'lat: ${widget.lat}, long: ${widget.lng}',
                  style: TextStyle(color: Colors.black, fontSize: 12),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 24.0),
                Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                  child: Material(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    elevation: 10.0,
                    clipBehavior: Clip.antiAlias,
                    color: Colors.blue,
                    child: MaterialButton(
                      minWidth: 180.0,
                      height: 60.0,
                      onPressed: () async {
                        provider.setName = nameController.text;
                        provider.setPIC = picController.text;
                        provider.setKontak = kontakController.text;
                        provider.setPengungsi = pengungsiController.text;
                        provider.setKebutuhan = kebutuhanController.text;

                        if (provider.getValidation) {
                          postMarker();
                        }
                      },
                      color: Colors.blueAccent[700],
                      child: Text(
                        'TAMBAH',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                )
              ]
            )
        )
    );
  }

  postMarker() async {
    MyDialog().dialogLoading(context);

    String token = await Session().getString(Session.keyTokenAuth) ?? "";

    var data = {
      "name": nameController.text,
      "lat": widget.lat.toString(),
      "lng": widget.lng.toString(),
      "disaster": widget.disasterId,
      "pic": picController.text,
      "kontak": kontakController.text,
      "pengungsi": pengungsiController.text,
      "kebutuhan": kebutuhanController.text,
    };

    var jsonResponse = await Api().methodPostPostman(Api.postMarker, token, data);

    if (jsonResponse != null) {
      Navigator.of(context).pop();

      MyDialog().dialogSuccess(context, "Berhasil", "Data berhasil ditambahkan");
    } else {
      Navigator.of(context).pop();

      MyDialog().dialogNormal(context, "Gagal tambah data", "Harap periksa kembali data Anda");
    }
  }
}
