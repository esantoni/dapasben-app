import 'package:dapasben/provider/provider_add_disaster.dart';
import 'package:dapasben/utils/api.dart';
import 'package:dapasben/utils/my_dialog.dart';
import 'package:dapasben/utils/sessions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddDisaster extends StatefulWidget {
  @override
  _AddDisasterState createState() => _AddDisasterState();
}

class _AddDisasterState extends State<AddDisaster> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController descController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    final provider = Provider.of<ProviderAddDisaster>(context, listen: false);
    provider.setReset();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderAddDisaster>(context, listen: true);
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        title: Text('Data Pasca Bencana'),
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        child: ListView(
          children: [
            Text(
              "TAMBAH DISASTER",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: nameController,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                hintText: 'Nama',
                fillColor: Colors.white,
                filled: true,
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide(
                    color: Colors.white,
                    width: 2.0,
                  ),
                ),
                errorText: provider.getName == ""
                    ? "Harap masukan Nama"
                    : null,
              ),
            ),
            SizedBox(height: 8.0),
            TextField(
              controller: descController,
              textInputAction: TextInputAction.newline,
              keyboardType: TextInputType.multiline,
              minLines: 5,
              maxLines: 50,
              decoration: InputDecoration(
                hintText: 'Deskripsi',
                fillColor: Colors.white,
                filled: true,
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide(
                    color: Colors.white,
                    width: 2.0,
                  ),
                ),
                errorText: provider.getDesc == ""
                    ? "Harap masukan Deskripsi"
                    : null,
              ),
            ),
            SizedBox(height: 24.0),
            Padding(
              padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
              child: Material(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                elevation: 10.0,
                clipBehavior: Clip.antiAlias,
                color: Colors.blue,
                child: MaterialButton(
                  minWidth: 180.0,
                  height: 60.0,
                  onPressed: () async {
                    provider.setName = nameController.text;
                    provider.setDesc = descController.text;

                    if (provider.getValidation) {
                      postDisaster();
                    }
                  },
                  color: Colors.blueAccent[700],
                  child: Text(
                    'TAMBAH',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  postDisaster() async {
    MyDialog().dialogLoading(context);

    String token = await Session().getString(Session.keyTokenAuth) ?? "";

    var headers = {
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json"
    };

    var data = {
      "name": nameController.text,
      "desc": descController.text
    };

    var jsonResponse = await Api().methodPostJson(Api.postDisaster, headers, data);

    if (jsonResponse != null) {
      Navigator.of(context).pop();

      MyDialog().dialogSuccess(context, "Berhasil", "Data berhasil ditambahkan");
    } else {
      Navigator.of(context).pop();

      MyDialog().dialogNormal(context, "Gagal tambah data", "Harap periksa kembali data Anda");
    }
  }
}
