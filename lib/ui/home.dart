import 'package:dapasben/models/model_disaster.dart';
import 'package:dapasben/provider/provider_home.dart';
import 'package:dapasben/ui/add_disaster.dart';
import 'package:dapasben/ui/detail_disaster.dart';
import 'package:dapasben/utils/api.dart';
import 'package:dapasben/utils/my_dialog.dart';
import 'package:dapasben/utils/sessions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<ModelDisaster> listItems = [];

  @override
  void initState() {
    super.initState();
    final provider = Provider.of<ProviderHome>(context, listen: false);
    provider.setReset();

    getData();
  }

  Future<Null> getData() async {
    final provider = Provider.of<ProviderHome>(context, listen: false);

    String token = await Session().getString(Session.keyTokenAuth) ?? "";

    var jsonResponse = await Api().methodGet(Api.getDisaster, token);

    if (jsonResponse != null) {
      var jsonData = jsonResponse['data'];

      listItems = [];
      for (var d in jsonData) {
        listItems.add(ModelDisaster(
            d["ID"].toString(),
            d["CreatedAt"],
            d["name"],
            d["desc"]
        ));
      }
    } else {
      MyDialog().dialogNormal(context, "Gagal Ambil Data", "Silakan ulangi beberapa saat lagi");
    }

    provider.setIsLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderHome>(context, listen: true);
    return WillPopScope(
        child: Scaffold(
            backgroundColor: Colors.indigo[50],
            appBar: AppBar(
              title: Text('Data Pasca Bencana'),
              actions: [
                IconButton(
                  icon: Icon(Icons.power_settings_new, color: Colors.white, size: 24.0),
                  onPressed: () {
                    MyDialog().dialogLogout(
                        context,
                        "Keluar Akun",
                        "Anda yakin akan keluar dari akun Anda?");
                  },
                )
              ],
            ),
            body: Container(
              child: provider.getIsLoading ?
              RefreshIndicator(
                  child: listItems.length != 0 ?
                  ListView.builder(
                    padding: EdgeInsets.all(8),
                    itemCount: listItems.length,
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailDisaster(
                            disasterId: listItems[index].id
                        )));
                      },
                      child: Container(
                        margin: EdgeInsets.only(bottom: 8),
                        padding: EdgeInsets.only(top: 8, bottom: 8, right: 5, left: 5),
                        color: Colors.amber[index%2==0 ? 500 : 100],
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              listItems[index].name,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                            Text(
                                listItems[index].desc,
                                textAlign: TextAlign.center
                            ),
                          ],
                        ),
                      ),
                    ),
                  ) :
                  Container(
                      width: double.infinity,
                      height: double.infinity,
                      padding: EdgeInsets.all(50.0),
                      child: Wrap(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Kamu belum punya data disaster",
                                  style: TextStyle(
                                    color: Color(0xFF5B5E61),
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textAlign: TextAlign.center)
                            ],
                          )
                        ],
                      )),
                  onRefresh: getData
              ) :
              Container(
                  width: double.infinity,
                  height: double.infinity,
                  alignment: Alignment.center,
                  child: Wrap(
                    children: [
                      Column(
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(height: 15.0),
                          Text("Harap tunggu...",
                              style: TextStyle(
                                color: Color(0xFF272A2D),
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center)
                        ],
                      )
                    ],
                  )
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddDisaster()));
              },
              child: Icon(Icons.add),
            )
        ),
        onWillPop: () {
          MyDialog().dialogExitApps(
              context,
              "Keluar Aplikasi",
              "Anda yakin akan keluar dari aplikasi?");
          return;
        },
    );
  }
}
