import 'package:dapasben/provider/provider_login.dart';
import 'package:dapasben/ui/home.dart';
import 'package:dapasben/utils/api.dart';
import 'package:dapasben/utils/my_dialog.dart';
import 'package:dapasben/utils/sessions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    final provider = Provider.of<ProviderLogin>(context, listen: false);
    provider.setReset();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderLogin>(context, listen: true);
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 40.0, right: 40.0),
          children: [
            Hero(
              tag: 'hero',
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 90.0,
                child: Image.asset('assets/logos/logo.png'),
              ),
            ),
            SizedBox(height: 48.0),
            TextField(
              controller: usernameController,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                hintText: 'Username',
                fillColor: Colors.white,
                filled: true,
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide(
                    color: Colors.white,
                    width: 2.0,
                  ),
                ),
                errorText: provider.getUsername == ""
                    ? "Harap masukan Username Anda"
                    : null,
              ),
            ),
            SizedBox(height: 8.0),
            TextField(
              obscureText: provider.getIsHidden,
              controller: passwordController,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  hintText: 'Password',
                  contentPadding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  errorText: provider.getPassword == ""
                      ? "Harap masukan Password Anda"
                      : null,
                  suffixIcon: IconButton(
                    icon: provider.getIsHidden
                        ? Icon(Icons.visibility, color: Colors.black26)
                        : Icon(Icons.visibility_off, color: Colors.black26),
                    onPressed: () {
                      provider.setIsHidden = !provider.getIsHidden;
                    },
                  )),
            ),
            SizedBox(height: 24.0),
            Padding(
              padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
              child: Material(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                elevation: 10.0,
                clipBehavior: Clip.antiAlias,
                color: Colors.blue,
                child: MaterialButton(
                  minWidth: 180.0,
                  height: 60.0,
                  onPressed: () async {
                    provider.setUsername = usernameController.text;
                    provider.setPassword = passwordController.text;

                    if (provider.getValidation) {
                      postLogin();
                    }
                  },
                  color: Colors.blueAccent[700],
                  child: Text(
                    'LOGIN',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  postLogin() async {
    MyDialog().dialogLoading(context);

    var data = {
      "username": usernameController.text,
      "password": passwordController.text
    };

    var jsonResponse = await Api().methodPost(Api.postLogin, data);

    if (jsonResponse != null) {
      Session().addInt(Session.keyId, jsonResponse['id']);
      Session().addString(Session.keyFullname, jsonResponse['fullname']);
      Session().addString(Session.keyTokenAuth, jsonResponse['token']);

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Home()),
          (Route<dynamic> route) => false);
    } else {
      Navigator.of(context).pop();

      MyDialog().dialogNormal(context, "Gagal Login", "Silakan cek data Anda kembali");
    }
  }
}
