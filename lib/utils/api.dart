import 'dart:convert' as convert;
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class Api {
  static String host = "http://dapasben.reshaantoni.my.id/api";

  static String postLogin = host + "/login";
  static String getDisaster = host + "/disasters";
  static String postDisaster = host + "/disaster";
  static String getMarker = host + "/markers";
  static String postMarker = host + "/marker";

  methodPost(String url, Map<String, dynamic> data) async {
    try {
      var response = await http.post(url, body: data).timeout(Duration(seconds: 30));
      var jsonResponse = convert.jsonDecode(response.body);

      print(data.toString());
      print(response.statusCode.toString());
      print(response.body.toString());

      return jsonResponse;
    } catch (e) {
      print('General Error: $e');
      return null;
    }
  }

  methodGet(String url, String token) async {
    try {
      var response = await http.get(url, headers: {
        HttpHeaders.authorizationHeader: "Bearer " + token
      }).timeout(Duration(seconds: 30));
      var jsonResponse = convert.jsonDecode(response.body);

      print(response.statusCode.toString());
      print(response.body.toString());

      return jsonResponse;
    } catch (e) {
      print('General Error: $e');
      return null;
    }
  }

  methodPostJson(String url, Map<String, dynamic> headers, Map<String, dynamic> data) async {
    try {
      var response = await http.post(url, body: json.encode(data), headers: headers).timeout(Duration(seconds: 30));
      var jsonResponse = convert.jsonDecode(response.body);

      print(headers.toString());
      print(json.encode(data));
      print(response.statusCode.toString());
      print(response.body.toString());

      if (response.statusCode == 200){
        return jsonResponse;
      }else{
        return null;
      }
    } catch (e) {
      print('General Error: $e');
      return null;
    }
  }

  methodPostPostman(String url, String auth, Map<String, String> data) async {
    try {
      var headers = {
        'Authorization': 'Bearer $auth'
      };
      var request = http.MultipartRequest('POST', Uri.parse(url));
      request.fields.addAll(data);
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      var jsonResponse = convert.jsonDecode(await response.stream.bytesToString());

      print(headers.toString());
      print(json.encode(data));
      print(response.statusCode.toString());
      print(jsonResponse.toString());

      if (response.statusCode == 200) {
        return jsonResponse;
      } else {
        return null;
      }
    } catch (e) {
      print('General Error: $e');
      return null;
    }
  }
}