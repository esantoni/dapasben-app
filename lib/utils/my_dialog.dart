import 'package:dapasben/ui/add_marker.dart';
import 'package:dapasben/ui/home.dart';
import 'package:dapasben/ui/login.dart';
import 'package:dapasben/utils/sessions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyDialog {
  dialogLoading(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      child: AlertDialog(
        title: Row(mainAxisSize: MainAxisSize.min, children: [
          CircularProgressIndicator(),
          SizedBox(width: 10.0),
          Text(
            "Harap tunggu...",
            style: TextStyle(color: Color(0xFF272A2D), fontSize: 15.0),
          ),
        ]),
      ),
    );
  }

  dialogNormal(BuildContext context, String title, String text) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => WillPopScope(
            child: AlertDialog(
              title: Text(
                title,
                style: TextStyle(
                    color: Color(0xFF272A2D),
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold),
              ),
              content: Text(
                text,
                style: TextStyle(color: Color(0xFF272A2D), fontSize: 14.0),
              ),
              actions: [
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "Oke",
                      style: TextStyle(
                          color: Color(0xFF257AC0),
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold),
                    ))
              ],
            ),
            onWillPop: () async => false));
  }

  dialogSuccess(BuildContext context, String title, String text) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => WillPopScope(
            child: AlertDialog(
              title: Text(
                title,
                style: TextStyle(
                    color: Color(0xFF272A2D),
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold),
              ),
              content: Text(
                text,
                style: TextStyle(color: Color(0xFF272A2D), fontSize: 14.0),
              ),
              actions: [
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => Home()),
                              (Route<dynamic> route) => false);
                    },
                    child: Text(
                      "Oke",
                      style: TextStyle(
                          color: Color(0xFF257AC0),
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold),
                    ))
              ],
            ),
            onWillPop: () async => false));
  }

  dialogLogout(BuildContext context, String title, String text) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: AlertDialog(
          title: Text(
            title,
            style: TextStyle(
                color: Color(0xFF272A2D),
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          content: Wrap(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    text,
                    style: TextStyle(
                        color: Color(0xFF272A2D),
                        fontSize: 14.0),
                  ),
                  SizedBox(height: 15.0),
                  Row(
                    children: [
                      Flexible(
                          flex: 1,
                          child: Container(
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              color: Color(0xFFFFFFFF),
                              child: Text(
                                "Batal",
                                style: TextStyle(
                                    color: Color(0xFF2C9EE9),
                                    fontSize: 16.0),
                              ),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(width: 1, color: Color(0xFF2C9EE9)),
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                          )),
                      SizedBox(width: 10.0),
                      Flexible(
                          flex: 1,
                          child: Container(
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                Session().clear();
                                Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (context) => Login()),
                                        (Route<dynamic> route) => false);
                              },
                              color: Color(0xFF2C9EE9),
                              child: Text(
                                "Keluar",
                                style: TextStyle(
                                    color: Color(0xFFFFFFFF),
                                    fontSize: 16.0),
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                          ))
                    ],
                  )
                ],
              )
            ],
          ),
        ));
  }

  dialogExitApps(BuildContext context, String title, String text) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: AlertDialog(
          title: Text(
            title,
            style: TextStyle(
                color: Color(0xFF272A2D),
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          content: Wrap(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    text,
                    style: TextStyle(
                        color: Color(0xFF272A2D),
                        fontSize: 14.0),
                  ),
                  SizedBox(height: 15.0),
                  Row(
                    children: [
                      Flexible(
                          flex: 1,
                          child: Container(
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              color: Color(0xFFFFFFFF),
                              child: Text(
                                "Batal",
                                style: TextStyle(
                                    color: Color(0xFF2C9EE9),
                                    fontSize: 16.0),
                              ),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(width: 1, color: Color(0xFF2C9EE9)),
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                          )),
                      SizedBox(width: 10.0),
                      Flexible(
                          flex: 1,
                          child: Container(
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                SystemNavigator.pop();
                              },
                              color: Color(0xFF2C9EE9),
                              child: Text(
                                "Keluar",
                                style: TextStyle(
                                    color: Color(0xFFFFFFFF),
                                    fontSize: 16.0),
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                          ))
                    ],
                  )
                ],
              )
            ],
          ),
        ));
  }

  dialogAddLocation(BuildContext context, String title, String text, double lat, double lng, String disasterId) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: AlertDialog(
          title: Text(
            title,
            style: TextStyle(
                color: Color(0xFF272A2D),
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          content: Wrap(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    text,
                    style: TextStyle(
                        color: Color(0xFF272A2D),
                        fontSize: 14.0),
                  ),
                  SizedBox(height: 15.0),
                  Row(
                    children: [
                      Flexible(
                          flex: 1,
                          child: Container(
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              color: Color(0xFFFFFFFF),
                              child: Text(
                                "Batal",
                                style: TextStyle(
                                    color: Color(0xFF2C9EE9),
                                    fontSize: 16.0),
                              ),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(width: 1, color: Color(0xFF2C9EE9)),
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                          )),
                      SizedBox(width: 10.0),
                      Flexible(
                          flex: 1,
                          child: Container(
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddMarker(
                                    disasterId: disasterId,
                                    lat: lat,
                                    lng: lng,
                                )));
                              },
                              color: Color(0xFF2C9EE9),
                              child: Text(
                                "Tambah",
                                style: TextStyle(
                                    color: Color(0xFFFFFFFF),
                                    fontSize: 16.0),
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                          ))
                    ],
                  )
                ],
              )
            ],
          ),
        ));
  }
}
